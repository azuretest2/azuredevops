#CREATE ITWO40FS DIRECTORY
New-Item -Path 'C:\itwo40fs\' -ItemType Directory

#SHARE DIRECTORY WITH ADMINISTRATOR FULL ACCESS
New-SMBShare -Name itwo40fs `
-Path C:\itwo40fs\ `
-FullAccess  Administrators

#INSTALL .NET CORE HOSTING BUNDLE 2.1
Invoke-WebRequest https://download.visualstudio.microsoft.com/download/pr/99800e2a-a5c0-4340-9379-e911e60fb879/1582a935b900bc4c8e337b594e8e7d56/dotnet-hosting-3.1.20-win.exe -OutFile 'dotnet-hosting-3.1.20-win.exe'

Invoke-WebRequest https://download.visualstudio.microsoft.com/download/pr/d144f312-0922-4c92-a13f-9ffdf946525e/f5fd0de3cc3a88ba6bdb515e6e4dc41a/dotnet-sdk-3.1.409-win-x64.exe -OutFile 'dotnet-sdk-3.1.409-win-x64.exe'

Start-Process -FilePath ".\dotnet-hosting-3.1.20-win" -ArgumentList /S, /qn -Wait

Start-Process -FilePath ".\dotnet-sdk-3.1.409-win-x64" -ArgumentList /S, /qn -Wait

#Install Basic IIS
Install-WindowsFeature -name Web-Server -IncludeManagementTools

#Invoke-WebRequest https://gitlab.com/azuretest2/azuredevops/-/raw/main/itwo40_4.3.0_bld_4.3.00123.00010843_date_14092021-1458_Update_C_1_.exe?inline=false -OutFile 'C:\itwo40fs\itwo40_4.3.0_bld_4.3.00123.00010843_date_14092021-1458_Update_C_1_.exe'
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) 
choco install urlrewrite -y
choco install nodejs.install --version="14.16.1"
choco install nodejs --version="14.16.1"


