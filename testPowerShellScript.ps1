New-Item -Path 'C:\temp\' -ItemType Directory
New-Item  C:\temp\finalps.ps1
Set-Content C:\temp\finalps.ps1 '$connectTestResult = Test-NetConnection -ComputerName $storageAccountName.file.core.windows.net -Port 445
if ($connectTestResult.TcpTestSucceeded) {
    # Save the password so the drive will persist on reboot
    cmd.exe /C "cmdkey /add:`"$storageAccountName.file.core.windows.net`" /user:`"localhost\$storageAccountName`" /pass:`"$storageAccountKey`""
    # Mount the drive
    New-PSDrive -Name F -PSProvider FileSystem -Root "\\$storageAccountName.file.core.windows.net\itwofileshare" -Persist
} else {
    Write-Error -Message "Unable to reach the Azure storage account via port 445. Check to make sure your organization or ISP is not blocking port 445, or use Azure P2S VPN, Azure S2S VPN, or Express Route to tunnel SMB traffic over a different port."}'
$parameter1 = $args[0]
$parameter2 = $args[1]
(Get-Content  C:\temp\finalps.ps1).replace('$storageAccountKey', $parameter2) | Set-Content  C:\temp\finalps.ps1
(Get-Content  C:\temp\finalps.ps1).replace('$storageAccountName', $parameter1) | Set-Content  C:\temp\finalps.ps1
& C:\temp\finalps.ps1
